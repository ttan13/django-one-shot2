from django.urls import reverse_lazy
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todolist"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    # success_url = reverse_lazy("show_todolist")
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    # success_url = reverse_lazy("show_todolist")
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todoitem/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    # success_url = reverse_lazy("show_todolist")

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todoitem/update.html"
    context_object_name = "todoitem"

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])
